# Backend Test

### Cash Machine

Create a model to represent a User with the follow use cases:
* A user can has "N" accounts
* An account could be "credit" or "debit" type

* Initialize the user debit account with $1,000.00 amount
* An user can withdraw from debit account only if the user has the available amount otherwise an exception will occure
* An user can deposit to hes debit account

* Initialize the user credit account with $1,000.00 amount
* An user can withdraw from credit account with 10% fee only if the user has credit otherwise an exception will occure
* An user can pay for hes credit account only 

#### Note: feel free to design the ER model their relationships and attributes

## Requirement
* PHP >= 7.1
* Database MySQL or MariaDB
* A modern MVC Framework (Symfony, Laravel, Phalcon, etc...)
* File README.md with instructions to run the project

### Differential
* RestFul API
* Unit tests 
* Autocontained App (Docker)
* CI pipelines

### Evaluation criteria
* Creativity: The previous instructions do not limit any desire of the developer, feel free to porpuse
* Organization: project structure, versioning
* Good practices, Standards (PSRs, Linteners, etc)
* Technology: use of paradigms, frameworks and libraries
* Design patterns, OOP, S.O.L.I.D. principles
